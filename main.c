/* 
 * File:   main.c
 * Author: nicholas
 *
 * Created on 08 April 2013, 12:01
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#define peaking 0
#define notch   1

typedef struct {

    unsigned int fs;

    double Q;
    double Gain;
    double GainLinear;
    int Fc;
    int Type;

    long double A;
    long double w0;
    long double alpha;

    long double a0;
    long double a1;
    long double a2;
    long double b0;
    long double b1;
    long double b2;

    uint32_t a1hex;
    uint32_t a2hex;
    uint32_t b0hex;
    uint32_t b1hex;
    uint32_t b2hex;

} RCMFilter;

void setFilterValues(RCMFilter*, double, double, unsigned int, int);
void initDefaultFilterValues(RCMFilter*);

int main(int argc, char** argv) {

    RCMFilter rcmFilter1;
    RCMFilter* f1 = &rcmFilter1;
    
    initDefaultFilterValues(f1);

    setFilterValues(f1, 2.0, 1.0, 100, peaking);

    printf("\nw0 = %0.15f \n",f1->w0);
    printf("alpha = %0.15f \n",f1->alpha);
    printf("A = %0.15f \n",f1->A);

    printf("\na0 - %0.15f \n",f1->a0);
    printf("a1 - %0.15f \n",f1->a1);
    printf("a2 - %0.15f \n",f1->a2);
    printf("b0 - %0.15f \n",f1->b0);
    printf("b1 - %0.15f \n",f1->b1);
    printf("b2 - %0.15f \n",f1->b2);

    return (EXIT_SUCCESS);
}

void initDefaultFilterValues(RCMFilter* x)
{
    x->fs    = 48000;
    x->Gain  = 1;           // dB boost at Fc
    x->GainLinear = 0;      // Overall gain of filter
    x->Fc    = 100;
    x->Q     = 1;
    x->Type  = peaking;

    x->A     = 1;
    x->w0    = 1;
    x->alpha = 1;
    x->a0    = 1;
    x->a1    = 1;
    x->a2    = 1;
    x->b0    = 1;
    x->b1    = 1;
    x->b2    = 1;


}

void setFilterValues(RCMFilter* x, double newQ, double newGain, unsigned int newFc, int newType)
{
    x->Q    = newQ;
    x->Gain = newGain;
    x->Fc   = newFc;
    x->Type = newType;

    /* Calculate other data for current filter */
    x->w0 = (2 * 3.14159265358979 * x->Fc) / x->fs;
    x->A  = pow(10,(x->Gain/40)); 
    x->GainLinear = pow(10,(0/20));

    x->alpha = sin(x->w0) / (2 * x->A * x->Q);

    if(x->Type == peaking)
    {
        x->b0 = (1 + x->alpha * x->A) * x->GainLinear;
        x->b1 = -(2 * cos(x->w0)) * x->GainLinear;
        x->b2 = (1 - x->alpha * x->A) * x->GainLinear;

        x->a0 = 1 + x->alpha / x->A;
        x->a1 = -2 * cos(x->w0);
        x->a2 = 1 - x->alpha / x->A;     
    } 
    else if (x->Type == notch)
    {
        x->b0 = 1;
        x->b1 = -2 * cos(x->w0);
        x->b2 = 1;

        x->a0 = 1 + x->alpha;
        x->a1 = -2 * cos(x->w0);
        x->a2 = 1 - x->alpha;
    }

    /* Normalise to a0 */
    
    x->a1 = x->a1 / x->a0;
    x->a2 = x->a2 / x->a0;
    x->b1 = x->b1 / x->a0;
    x->b2 = x->b2 / x->a0;
    x->b0 = x->b0 / x->a0;
    x->a0 = x->a0 / x->a0;

    /* Invert a1 and a2 to match SigmaDSP parameter RAM expectations */
    x->a1 = -x->a1;
    x->a2 = -x->a2;

    x->a1hex = x->a1 * pow(2,23);
    x->a2hex = x->a2 * pow(2,23);
    x->b0hex = x->b0 * pow(2,23);
    x->b1hex = x->b1 * pow(2,23);
    x->b2hex = x->b2 * pow(2,23);
}

